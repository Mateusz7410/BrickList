package babiaczyk.mateusz.bricklist;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;

public class ItemAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<ProjectItem> mDataSource;
    private DatabaseAccess db;

    public ItemAdapter(Context context, ArrayList<ProjectItem> items, DatabaseAccess db) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.db = db;
    }
    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        // check if the view already exists if so, no need to inflate and findViewById again!
        if (convertView == null) {

            // Inflate the custom row layout from your XML.
            convertView = mInflater.inflate(R.layout.project_item, parent, false);

            // create a new "Holder" with subviews
            holder = new ViewHolder();
            holder.itemIMG = (ImageView) convertView.findViewById(R.id.itemIMG);
            holder.itemName = (TextView) convertView.findViewById(R.id.itemName);
            holder.itemMissingCount = (TextView) convertView.findViewById(R.id.missingPartsNumber);
            holder.itemMissingCountText = (TextView) convertView.findViewById(R.id.missingPartsTextView);

            // hang onto this holder for future recyclage
            convertView.setTag(holder);
        }
        else {

            // skip all the expensive inflation/findViewById and just get the holder you already made
            holder = (ViewHolder) convertView.getTag();
        }

        // Get relevant subviews of row view
        TextView itemName = holder.itemName;
        TextView itemMissingCount = holder.itemMissingCount;
        TextView itemMissingCountText = holder.itemMissingCountText;
        final ImageView itemIMG = holder.itemIMG;

        //Get corresponding recipe for row
        final ProjectItem item = (ProjectItem) getItem(position);

        // Update row view's textviews to display recipe information
        itemName.setText(item.name);
        //Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/Legothick.ttf");
        //itemName.setTypeface(typeface);
        itemMissingCount.setText(item.qty);
        itemMissingCountText.setText("Ilość brakujących klocków: ");

        // Use Picasso to load the image. Temporarily have a placeholder in case it's slow to load
        //Picasso.with(mContext).setLoggingEnabled(true);
        itemIMG.setImageResource(R.mipmap.ic_photo);
        if(item.image != null){
            itemIMG.setImageBitmap(item.image);
        }else{

            Target target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    holder.itemIMG.setImageBitmap(bitmap);
                    db.insertProjectImage(bitmap, item.name);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };
            Picasso.with(mContext).load(item.url).error(R.mipmap.ic_photo).placeholder(R.mipmap.ic_photo).into(target);

        }



        return convertView;
    }

    private static class ViewHolder {
        public TextView itemName;
        public TextView itemMissingCount;
        public TextView itemMissingCountText;
        public ImageView itemIMG;
    }


}
