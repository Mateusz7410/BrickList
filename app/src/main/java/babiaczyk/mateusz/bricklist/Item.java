package babiaczyk.mateusz.bricklist;

public class Item {
    public String type;
    public String id;
    public String qty;
    public String qtyStore;
    public String color;
    public String extra;
    public int partID;
}
