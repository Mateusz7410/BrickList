package babiaczyk.mateusz.bricklist;

import android.graphics.Bitmap;

public class PartItem {
    public String name;
    public String imgUrl;
    public String quantityInSetNumber;
    public String quantityInStoreNumber;
    public String color;
    public int partID;
    public Bitmap image;
}
