package babiaczyk.mateusz.bricklist;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class PartsList extends AppCompatActivity {

    TextView appName;
    ImageView back;
    ImageView archive;
    ImageView export;
    TextView projectName;
    ListView listView;
    DatabaseAccess db;
    ArrayList<Item> items;
    ArrayList<PartItem> partItems;
    int projectID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_parts_list);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        db = DatabaseAccess.getInstance(this);
        db.open();


        appName = (TextView) findViewById(R.id.appName);
        projectName = (TextView) findViewById(R.id.projectName);
        back = (ImageView) findViewById(R.id.back);
        archive = (ImageView) findViewById(R.id.archive);
        export = (ImageView) findViewById(R.id.export);
        listView = (ListView) findViewById(R.id.item_list_view);

        export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(PartsList.this, R.style.MyAlertDialogStyle);

                builder.setTitle("Eksportuj");
                builder.setMessage("Czy na pewno chcesz wyeksportować brakujące elementy z tego projektu do pliku XML?\nPlik znajdzie się w folderze 'BrickList' o nazwie: " + projectName.getText().toString() + ".xml");

                builder.setPositiveButton("TAK", new android.content.DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing but close the dialog
                        BufferedWriter writer = null;
                        try {
                            File f = new File("/storage/emulated/0/BrickList/");
                            f.mkdirs();
                            File file = new File(f, projectName.getText().toString() + ".xml");


                            // This will output the full path where the file will be written to...
                            Log.e("alala", file.getCanonicalPath());

                            writer = new BufferedWriter(new FileWriter(file));
                            StringBuilder sb = new StringBuilder();
                            sb.append("<INVENTORY>\n");

                            for(Item item: items){
                                xmlStructure xml = db.getXmlData(item.partID);
                                if(Integer.parseInt(xml.qtyFilled )> 0){
                                    sb.append("  <ITEM>\n");
                                    sb.append("      <ITEMTYPE>" + xml.itemType + "</ITEMTYPE>\n");
                                    sb.append("      <ITEMID>" + xml.itemID + "</ITEMID>\n");
                                    sb.append("      <COLOR>" + xml.color + "</COLOR>\n");
                                    sb.append("      <QTYFILLED>" + xml.qtyFilled + "</QTYFILLED>\n");
                                    sb.append("      <EXTRA>" + xml.extra + "</EXTRA>\n");
                                    sb.append("  </ITEM>\n");
                                }
                            }
                            sb.append("</INVENTORY>");

                            writer.write(sb.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                // Close the writer regardless of what happens...
                                writer.close();
                            } catch (Exception e) {
                            }
                        }
                        Toast.makeText(getApplicationContext(), "Wyeksportowano plik!", Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton("ANULUJ", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Do nothing

                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();




            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        archive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(PartsList.this, R.style.MyAlertDialogStyle);

                builder.setTitle("Archiwizuj");
                builder.setMessage("Czy na pewno chcesz zarchiwizować ten projekt?");

                builder.setPositiveButton("TAK", new android.content.DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing but close the dialog
                        db.archiveProject(projectID);
                        Toast.makeText(getApplicationContext(), "Zarchiwizowano projekt!", Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton("ANULUJ", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Do nothing

                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();

            }
        });

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Legothick.ttf");
        appName.setTypeface(typeface);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        String value = extras.getString("projectName");
        projectName.setText(value);
        projectID = extras.getInt("projectID");
        if(extras.getInt("active") == 0){
            archive.setVisibility(View.GONE);
        }

        items = db.getInventoryParts(projectID);
        /*for(Item item: items){
            Log.e("alala", item.id + " " + item.qty + " " +item.qtyStore + " " +item.color + " " +item.extra + " " +item.type);
        }*/
        partItems = db.getPartsItems(items);


        PartItemAdapter partItemAdapter = new PartItemAdapter(this, partItems, db);
        listView.setAdapter(partItemAdapter);

    }

    @Override
    public void finish() {
        db.updateLastAccessed(projectID);
        db.close();
        super.finish();
    }
}
