package babiaczyk.mateusz.bricklist;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.provider.Telephony;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;

public class PartItemAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<PartItem> mDataSource;
    private View globalConvertView;
    private DatabaseAccess db;
    private boolean[] ready;

    public PartItemAdapter(Context context, ArrayList<PartItem> items, DatabaseAccess db) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ready = new boolean[mDataSource.size()];
        int i=0;
        for(PartItem item:items){
            if(item.quantityInStoreNumber == item.quantityInSetNumber){
                ready[i] = true;
            }else{
                ready[i] = false;
            }
        }
        this.db = db;
    }
    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final PartItem item = (PartItem) getItem(position);
        // check if the view already exists if so, no need to inflate and findViewById again!
        if (convertView == null) {

            // Inflate the custom row layout from your XML.
            convertView = mInflater.inflate(R.layout.part_item, parent, false);
            globalConvertView = convertView;
            // create a new "Holder" with subviews
            holder = new ViewHolder();
            holder.itemIMG = (ImageView) convertView.findViewById(R.id.itemIMG);
            holder.itemName = (TextView) convertView.findViewById(R.id.itemName);
            holder.quantityInSetNumber = (TextView) convertView.findViewById(R.id.SetNumber);
            holder.quantityInStoreNumber = (TextView) convertView.findViewById(R.id.StoreNumber);
            holder.color = (TextView) convertView.findViewById(R.id.colorName);
            holder.number = (EditText) convertView.findViewById(R.id.number);
            holder.add = (Button) convertView.findViewById(R.id.increment);
            holder.delete = (Button) convertView.findViewById(R.id.decrement);
            holder.linearLayout = (LinearLayout) convertView.findViewById(R.id.linearLayout);

            // hang onto this holder for future recyclage
            convertView.setTag(holder);
        }
        else {

            // skip all the expensive inflation/findViewById and just get the holder you already made
            holder = (ViewHolder) convertView.getTag();
        }

        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int actual = Integer.parseInt(holder.quantityInStoreNumber.getText().toString());
                int max = Integer.parseInt(holder.quantityInSetNumber.getText().toString());
                int inEditBox;
                if(holder.number.getText().toString().equals(""))
                    inEditBox = 1;
                else
                    inEditBox = Integer.parseInt(holder.number.getText().toString());

                if(inEditBox + actual > max){
                    Toast.makeText(mContext, "Nie możesz dodać takiej ilości klocków!", Toast.LENGTH_SHORT).show();
                    holder.number.setText("");
                }else if(inEditBox <= 0){
                    Toast.makeText(mContext, "Podano niepoprawną ilość klocków!", Toast.LENGTH_SHORT).show();
                    holder.number.setText("");
                }else{
                    db.addToStoreNumber(inEditBox, actual, item.partID);
                    //Log.e("alalal", Integer.toString(item.partID) + " " + Integer.toString(actual) + " " +Integer.toString(inEditBox)+" " + item.imgUrl );
                    item.quantityInStoreNumber = Integer.toString(actual + inEditBox);
                    holder.quantityInStoreNumber.setText(Integer.toString(actual + inEditBox));
                    holder.number.setText("");
                }
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int actual = Integer.parseInt(holder.quantityInStoreNumber.getText().toString());
                int max = Integer.parseInt(holder.quantityInSetNumber.getText().toString());
                int inEditBox;
                if(holder.number.getText().toString().equals(""))
                    inEditBox = 1;
                else
                    inEditBox = Integer.parseInt(holder.number.getText().toString());

                if( actual - inEditBox < 0){
                    Toast.makeText(mContext, "Nie możesz odjąć takiej ilości klocków!", Toast.LENGTH_SHORT).show();
                    holder.number.setText("");
                }else if(inEditBox <= 0){
                    Toast.makeText(mContext, "Podano niepoprawną ilość klocków!", Toast.LENGTH_SHORT).show();
                    holder.number.setText("");
                }else{
                    db.minusToStoreNumber(inEditBox, actual, item.partID);
                    //Log.e("alalal", Integer.toString(item.partID) + " " + Integer.toString(actual) + " " +Integer.toString(inEditBox)+" " + item.imgUrl );
                    item.quantityInStoreNumber = Integer.toString(actual - inEditBox);
                    holder.quantityInStoreNumber.setText(Integer.toString(actual - inEditBox));
                    holder.number.setText("");

                }
            }
        });

        // Get relevant subviews of row view
        TextView itemName = holder.itemName;
        TextView quantityInSetNumber = holder.quantityInSetNumber;
        TextView quantityInStoreNumber = holder.quantityInStoreNumber;
        TextView color = holder.color;
        final ImageView itemIMG = holder.itemIMG;

        Button delete = (Button) convertView.findViewById(R.id.decrement);
        Button add = (Button) convertView.findViewById(R.id.increment);
        delete.getBackground().setColorFilter(Color.parseColor("#FF420E"), PorterDuff.Mode.MULTIPLY);
        add.getBackground().setColorFilter(Color.parseColor("#80bd9e"), PorterDuff.Mode.MULTIPLY);

        //Get corresponding recipe for row


        // Update row view's textviews to display recipe information
        itemName.setText(item.name);

        quantityInSetNumber.setText(item.quantityInSetNumber);
        quantityInStoreNumber.setText(item.quantityInStoreNumber);
        color.setText(item.color);



        // Use Picasso to load the image. Temporarily have a placeholder in case it's slow to load
        //Picasso.with(mContext).setLoggingEnabled(true);

        itemIMG.setImageResource(R.mipmap.ic_photo);
        if(item.image != null){
            itemIMG.setImageBitmap(item.image);
        }else {

            Target target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    holder.itemIMG.setImageBitmap(bitmap);
                    item.image = bitmap;
                    if(db != null)
                        db.insertItemImage(bitmap, item.partID);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };
            Picasso.with(mContext).load(item.imgUrl).error(R.mipmap.ic_photo).placeholder(R.mipmap.ic_photo).into(target);
        }


        return convertView;
    }

    private static class ViewHolder {
        public TextView itemName;
        public TextView quantityInSetNumber;
        public TextView quantityInStoreNumber;
        public TextView color;
        public ImageView itemIMG;
        public EditText number;
        public Button add;
        public Button delete;
        public LinearLayout linearLayout;
    }
}
