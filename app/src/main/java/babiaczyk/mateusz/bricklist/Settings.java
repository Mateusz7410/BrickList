package babiaczyk.mateusz.bricklist;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.support.v7.app.AlertDialog;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

public class Settings extends AppCompatActivity {
    TextView appName;
    Button save;
    TextView opcjeText;
    TextView extraTextView;
    EditText prefixURL;
    TextView prefixes;
    TextView archiveTextView;
    DatabaseAccess db;
    Button choose;
    Button addUrl;
    Button deleteUrl;
    Button archive;
    LinearLayout mainWindow;
    Spinner s;
    Button extra;
    private Context mContext;
    private PopupWindow mPopupWindow;
    private int extras = 0;
    private int archives = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_settings);
        db = DatabaseAccess.getInstance(this);
        db.open();
        Intent intent = getIntent();
        String value = intent.getStringExtra("prefixURL");
        appName = (TextView) findViewById(R.id.appName);
        save = (Button) findViewById(R.id.save);
        opcjeText = (TextView) findViewById(R.id.textView);
        extraTextView = (TextView) findViewById(R.id.extraTextView);
        prefixURL = (EditText) findViewById(R.id.prefixURL);
        prefixURL = (EditText) findViewById(R.id.prefixURL);
        prefixes = (TextView) findViewById(R.id.textView6);
        archiveTextView = (TextView) findViewById(R.id.archiveTextView);
        addUrl = (Button) findViewById(R.id.addUrl);
        choose = (Button) findViewById(R.id.choose);
        extra = (Button) findViewById(R.id.extra);
        s = (Spinner) findViewById(R.id.spinner);
        mContext = getApplicationContext();
        mainWindow = (LinearLayout) findViewById(R.id.mainWindow);
        deleteUrl = (Button) findViewById(R.id.deleteUrl);
        archive = (Button) findViewById(R.id.archive);



        int[] arr;
        arr = db.getSettings();
        extras = arr[0];
        archives = arr[1];

        Log.e("alala", Integer.toString(extras) + " " + Integer.toString(archives));


        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Legothick.ttf");
        appName.setTypeface(typeface);
        opcjeText.setTypeface(typeface);
        prefixes.setTypeface(typeface);
        extraTextView.setTypeface(typeface);
        archiveTextView.setTypeface(typeface);
        save.getBackground().setColorFilter(Color.parseColor("#80bd9e"), PorterDuff.Mode.MULTIPLY);
        choose.getBackground().setColorFilter(Color.parseColor("#80bd9e"), PorterDuff.Mode.MULTIPLY);
        addUrl.getBackground().setColorFilter(Color.parseColor("#80bd9e"), PorterDuff.Mode.MULTIPLY);
        deleteUrl.getBackground().setColorFilter(Color.parseColor("#FF420E"), PorterDuff.Mode.MULTIPLY);
        if(extras == 1){
            extra.setText("Aktywne");
            extra.getBackground().setColorFilter(Color.parseColor("#80bd9e"), PorterDuff.Mode.MULTIPLY);
        }else{
            extra.setText("Nieaktywne");
            extra.getBackground().setColorFilter(Color.parseColor("#FF420E"), PorterDuff.Mode.MULTIPLY);
        }
        if(archives == 1){
            archive.setText("Aktywne");
            archive.getBackground().setColorFilter(Color.parseColor("#80bd9e"), PorterDuff.Mode.MULTIPLY);
        }else{
            archive.setText("Nieaktywne");
            archive.getBackground().setColorFilter(Color.parseColor("#FF420E"), PorterDuff.Mode.MULTIPLY);
        }


        extra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(extras == 1){
                    extra.setText("Nieaktywne");
                    extra.getBackground().setColorFilter(Color.parseColor("#FF420E"), PorterDuff.Mode.MULTIPLY);
                }else{
                    extra.setText("Aktywne");
                    extra.getBackground().setColorFilter(Color.parseColor("#80bd9e"), PorterDuff.Mode.MULTIPLY);
                }
                extras = (extras+1)%2;

            }
        });

        archive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(archives == 1){
                    archive.setText("Nieaktywne");
                    archive.getBackground().setColorFilter(Color.parseColor("#FF420E"), PorterDuff.Mode.MULTIPLY);
                }else{
                    archive.setText("Aktywne");
                    archive.getBackground().setColorFilter(Color.parseColor("#80bd9e"), PorterDuff.Mode.MULTIPLY);
                }
                archives = (archives+1)%2;
            }
        });

        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefixURL.setText(s.getSelectedItem().toString());
            }
        });



        deleteUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!s.getSelectedItem().toString().equals("http://fcds.cs.put.poznan.pl/MyWeb/BL/")){
                    db.deleteUrl(s.getSelectedItem().toString());
                    refreshSpinner();
                } else{
                    Toast.makeText(mContext, "Nie można usunąć podstawowego URL'a", Toast.LENGTH_SHORT).show();
                }
            }
        });

        addUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);



                View customView = inflater.inflate(R.layout.new_url,null);
                mPopupWindow = new PopupWindow(
                        customView,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                );


                if(Build.VERSION.SDK_INT>=21){
                    mPopupWindow.setElevation(5.0f);
                }


                Button addButton = (Button) customView.findViewById(R.id.createButton);
                addButton.getBackground().setColorFilter(Color.parseColor("#F98866"), PorterDuff.Mode.MULTIPLY);

                Button closeButton = (Button) customView.findViewById(R.id.closeButton);

                TextView newUrl = (TextView) customView.findViewById(R.id.textView3);
                Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Legothick.ttf");
                newUrl.setTypeface(typeface);


                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPopupWindow.dismiss();
                    }
                });

                EditText url = (EditText) customView.findViewById(R.id.url);
                url.setTextIsSelectable(true);


                addButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        EditText url = (EditText) mPopupWindow.getContentView().findViewById(R.id.url);
                        if(!url.getText().toString().equalsIgnoreCase("")){
                            db.insertURL(url.getText().toString());
                            refreshSpinner();
                            mPopupWindow.dismiss();
                        }
                        else{
                            Toast.makeText(mContext, "Pole nie może być puste!", Toast.LENGTH_LONG).show();
                        }

                    }
                });

                mPopupWindow.setFocusable(true);
                mPopupWindow.showAtLocation(mainWindow, Gravity.CENTER,0,-100);
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("prefixURL", prefixURL.getText().toString());
                db.updateSettings(extras, archives);
                intent.putExtra("extra", extras);
                intent.putExtra("archive", archives);
                Log.e("alala3", Integer.toString(extras) + " " + Integer.toString(archives));
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });

        prefixURL.setText(value);

        refreshSpinner();


    }

    public void refreshSpinner(){
        ArrayList<String> arraySpinner = db.getURLs();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s.setAdapter(adapter);
    }

    @Override
    public void finish() {
        db.close();
        super.finish();
    }
}
