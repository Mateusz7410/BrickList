package babiaczyk.mateusz.bricklist;

public class Inventory {
    public String name;
    public int active;
    public int lastAccessed;
    public int missingQty;
    public String number;

    public Inventory(String name, int active, int lastAccessed, String number, int missingQty ){
        this.name = name;
        this.active = active;
        this.lastAccessed = lastAccessed;
        this.number = number;
        this.missingQty = missingQty;
    }
}
