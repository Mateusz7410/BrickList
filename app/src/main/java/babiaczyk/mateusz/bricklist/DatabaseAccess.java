package babiaczyk.mateusz.bricklist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.Telephony;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;

public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;
    private String prefixUrlLego = "https://www.lego.com/service/bricks/5/2/";
    private String prefixUrlBrickLink = "https://www.bricklink.com/";

    /**
     * Private constructor to aboid object creation from outside classes.
     *
     * @param context
     */
    private DatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    /**
     * Read all quotes from the database.
     *
     * @return a List of quotes
     */
    public ArrayList<ProjectItem> getInventories(String imageURL, boolean archive) {
        ArrayList<ProjectItem> list = new ArrayList<>();
        Cursor cursor;
        if(archive)
            cursor = database.rawQuery("SELECT * FROM Inventories where Active = 1 order by LastAccessed DESC", null);
        else
            cursor = database.rawQuery("SELECT * FROM Inventories order by LastAccessed DESC", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ProjectItem projectItem = new ProjectItem();
            projectItem.id = cursor.getInt(0);
            projectItem.active = cursor.getInt(2);
            projectItem.name = cursor.getString(1).toUpperCase();
            projectItem.url = imageURL + cursor.getString(4) + "-1.png";
            projectItem.qty = Integer.toString(cursor.getInt(5));
            if(!cursor.isNull(6)){
                byte[] imgByte = cursor.getBlob(6);
                projectItem.image = BitmapFactory.decodeByteArray(imgByte, 0, imgByte.length);
            }else
                projectItem.image = null;

            list.add(projectItem);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public void deleteUrl(String url){

        database.delete("URLs","Name=?",new String[]{url});

    }

    public void insertURL(String url){

        ContentValues contentValues = new ContentValues();
        contentValues.put("Name", url);
        database.insert("URLs", null, contentValues);

    }

    public ArrayList<String> getURLs() {
        ArrayList<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM URLs", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public long insertInventory(Inventory inventory){

        ContentValues contentValues = new ContentValues();
        contentValues.put("Name", inventory.name.toUpperCase());
        contentValues.put("Active", inventory.active);
        contentValues.put("LastAccessed", inventory.lastAccessed);
        contentValues.put("Number", inventory.number);
        contentValues.put("MissingQty", inventory.missingQty);
        long row = database.insert("Inventories", null, contentValues);

        return row;
    }

    public ArrayList<Item> getInventoryParts(int id){
        ArrayList<Item> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM InventoriesParts where InventoryID=?", new String[] {Integer.toString(id)});
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Item item = new Item();

            item.partID = cursor.getInt(0); //id
            item.type = Integer.toString(cursor.getInt(2)); //id
            item.id = Integer.toString(cursor.getInt(3));  //id
            item.qty = Integer.toString(cursor.getInt(4));  //val
            item.qtyStore = Integer.toString(cursor.getInt(5)); //val
            item.color = Integer.toString(cursor.getInt(6));  //id
            item.extra = Integer.toString(cursor.getInt(7));  //????
            list.add(item);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }


    public void insertInventoryPart(ArrayList<Item> items, long inventoryID){
        Cursor cursor;
        for(Item item: items){
            ContentValues contentValues = new ContentValues();

            cursor = database.rawQuery("select id from ItemTypes where Code = ?", new String[] {item.type});
            cursor.moveToFirst();
            contentValues.put("TypeID", cursor.getInt(0));
            int typeID = cursor.getInt(0);
            cursor.close();


            cursor = database.rawQuery("select id from Parts where Code = ?", new String[] {item.id});
            if(cursor.getCount() == 1){
                cursor.moveToFirst();
                contentValues.put("ItemID",cursor.getInt(0));
                cursor.close();
            }
            else{
                cursor.close();
                ContentValues tmp = new ContentValues();
                tmp.put("TypeID", typeID);
                tmp.put("Code", item.id);
                tmp.put("Name", "Lego Character");
                tmp.put("CategoryID", "10000");
                database.insert("Parts", null, tmp);
                cursor = database.rawQuery("select id from Parts where Code = ?", new String[] {item.id});
                cursor.moveToFirst();
                contentValues.put("ItemID",cursor.getInt(0));
                cursor.close();
            }



            cursor = database.rawQuery("select id from Colors where Code = ?", new String[] {item.color});
            cursor.moveToFirst();
            contentValues.put("ColorID", cursor.getInt(0));
            cursor.close();



            contentValues.put("InventoryID", inventoryID); //good
            contentValues.put("QuantityInSet", item.qty); //good
            contentValues.put("QuantityInStore", 0); //good
            if(item.extra.equals("Y"))
                contentValues.put("Extra", 1);
            else
                contentValues.put("Extra", 0);

            database.insert("InventoriesParts", null, contentValues);
        }

    }

    public ArrayList<PartItem> getPartsItems(ArrayList<Item> items){
        ArrayList<PartItem> list = new ArrayList<>();
        for(Item item: items){
            PartItem partItem = new PartItem();
            partItem.partID = item.partID;
            partItem.quantityInSetNumber = item.qty;
            partItem.quantityInStoreNumber = item.qtyStore;



            Cursor cursor;
            cursor = database.rawQuery("select Name from Parts where id = ?", new String[] {item.id});
            cursor.moveToFirst();
            if(item.extra.equals("1"))
                partItem.name = "EXTRA: " + cursor.getString(0);
            else
                partItem.name = cursor.getString(0);
            cursor.close();




            cursor = database.rawQuery("select Name from Colors where id = ?", new String[] {item.color});
            cursor.moveToFirst();
            partItem.color = cursor.getString(0);
            if(partItem.color.equalsIgnoreCase("(Not Applicable)"))
                partItem.color = "None";
            cursor.close();


            partItem.imgUrl = null;
            cursor = database.rawQuery("select Code, Image from Codes where ItemID = ? and ColorID = ?", new String[] {item.id, item.color});
            cursor.moveToFirst();
            if(cursor.getCount() == 1){
                partItem.imgUrl = prefixUrlLego + cursor.getString(0);
                if(!cursor.isNull(1)){
                    byte[] imgByte = cursor.getBlob(1);
                    partItem.image = BitmapFactory.decodeByteArray(imgByte, 0, imgByte.length);
                }else
                    partItem.image = null;
            }
            cursor.close();
            if(partItem.imgUrl == null){
                cursor = database.rawQuery("select Code from ItemTypes where id = ?", new String[] {item.type});
                cursor.moveToFirst();
                partItem.imgUrl = prefixUrlBrickLink + "ItemImage/" + cursor.getString(0) + "N/";
                cursor.close();

                cursor = database.rawQuery("select Code from Colors where id = ?", new String[] {item.color});
                cursor.moveToFirst();
                partItem.imgUrl += cursor.getString(0) + "/";
                cursor.close();

                cursor = database.rawQuery("select Code from Parts where id = ?", new String[] {item.id});
                cursor.moveToFirst();
                partItem.imgUrl += cursor.getString(0) + ".png";
                cursor.close();
                //Log.e("alala", partItem.imgUrl);
            }



            list.add(partItem);
        }
        return list;
    }

    public void updateLastAccessed(int projectID){
        ContentValues cv = new ContentValues();
        cv.put("LastAccessed", getMaxLastAccessed());
        database.update("Inventories",cv, "id="+Integer.toString(projectID), null);
    }

    public int getMaxLastAccessed(){
        Cursor cursor = database.rawQuery("select max(LastAccessed) from Inventories", null);
        cursor.moveToFirst();
        int val = cursor.getInt(0);
        cursor.close();
        return val + 1;
    }

    public void addToStoreNumber(int num, int store, int id){
        ContentValues cv = new ContentValues();
        cv.put("QuantityInStore", num + store);
        database.update("InventoriesParts",cv, "id="+Integer.toString(id), null);

        Cursor cursor = database.rawQuery("select InventoryID from InventoriesParts where id=?", new String[]{Integer.toString(id)});
        cursor.moveToFirst();
        int InventoryID = cursor.getInt(0);
        cursor.close();

        cursor = database.rawQuery("select MissingQty from Inventories where id=?", new String[]{Integer.toString(InventoryID)});
        cursor.moveToFirst();
        int missing = cursor.getInt(0);
        cursor.close();

        cv = new ContentValues();
        cv.put("MissingQty", missing - num);
        database.update("Inventories",cv, "id="+Integer.toString(InventoryID), null);
    }

    public void minusToStoreNumber(int num, int store, int id){
        ContentValues cv = new ContentValues();
        cv.put("QuantityInStore", store - num);
        database.update("InventoriesParts",cv, "id="+Integer.toString(id), null);

        Cursor cursor = database.rawQuery("select InventoryID from InventoriesParts where id=?", new String[]{Integer.toString(id)});
        cursor.moveToFirst();
        int InventoryID = cursor.getInt(0);
        cursor.close();

        cursor = database.rawQuery("select MissingQty from Inventories where id=?", new String[]{Integer.toString(InventoryID)});
        cursor.moveToFirst();
        int missing = cursor.getInt(0);
        cursor.close();

        cv = new ContentValues();
        cv.put("MissingQty", missing + num);
        database.update("Inventories",cv, "id="+Integer.toString(InventoryID), null);
    }

    public void archiveProject(int projectID){
        ContentValues cv = new ContentValues();
        cv.put("Active", 0);
        long tmp = database.update("Inventories",cv, "id="+Integer.toString(projectID), null);
    }

    public void updateSettings(int extra, int archive){
        ContentValues cv = new ContentValues();
        cv.put("EXTRA", extra);
        cv.put("ARCHIVE", archive);
        long tmp = database.update("Settings",cv, null, null);
        Log.e("alala2", Integer.toString(extra) + " " + Integer.toString(archive));
    }

    public int[] getSettings(){
        Cursor cursor = database.rawQuery("select EXTRA, ARCHIVE  from Settings", null);
        cursor.moveToFirst();
        int extra = cursor.getInt(0);
        int archive = cursor.getInt(1);
        cursor.close();
        int[] arr = new int[2];
        arr[0] = extra;
        arr[1] = archive;

        return arr;
    }

    public xmlStructure getXmlData (int partID){
        xmlStructure xml = new xmlStructure();
        Cursor cursor = database.rawQuery("select *  from InventoriesParts where id = ?", new String[]{Integer.toString(partID)});
        cursor.moveToFirst();
        int typeID = cursor.getInt(2);
        int itemID = cursor.getInt(3);
        int quantityInSet = cursor.getInt(4);
        int quantityInStore = cursor.getInt(5);
        int colorID = cursor.getInt(6);
        int Extra = cursor.getInt(7);
        cursor.close();

        cursor = database.rawQuery("select Code from ItemTypes where id = ?", new String[]{Integer.toString(typeID)});
        cursor.moveToFirst();
        xml.itemType = cursor.getString(0);
        cursor.close();

        cursor = database.rawQuery("select Code from Parts where id = ?", new String[]{Integer.toString(itemID)});
        cursor.moveToFirst();
        xml.itemID = cursor.getString(0);
        cursor.close();

        cursor = database.rawQuery("select Code from Colors where id = ?", new String[]{Integer.toString(colorID)});
        cursor.moveToFirst();
        xml.color = cursor.getString(0);
        cursor.close();

        if(Extra == 1)
            xml.extra = "Y";
        else
            xml.extra = "N";

        xml.qtyFilled = Integer.toString(quantityInSet - quantityInStore);

        return xml;
    }

    public boolean checkProjectName(String projectName){

        Cursor cursor = database.rawQuery("select count(*) from Inventories where Name = ?", new String[]{projectName.toUpperCase()});
        cursor.moveToFirst();
        int odp = cursor.getInt(0);
        cursor.close();
        if(odp == 0)
            return true;
        else
            return false;
    }


    public void insertItemImage(Bitmap bm, int partID){
        Cursor cursor = database.rawQuery("select ItemID, ColorID from InventoriesParts where id = ?", new String[]{Integer.toString(partID)});
        cursor.moveToFirst();
        int itemID = cursor.getInt(0);
        int colorID = cursor.getInt(1);
        cursor.close();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 0, stream);
        byte[] data = stream.toByteArray();

        ContentValues contentValues = new ContentValues();
        contentValues.put("Image", data);
        database.update("Codes",contentValues, "ItemID=? and ColorID=?", new String[]{Integer.toString(itemID), Integer.toString(colorID)});
    }

    public void insertProjectImage(Bitmap bm, String projectName){
        Cursor cursor = database.rawQuery("select id from Inventories where Name = ?", new String[]{projectName});
        cursor.moveToFirst();
        int projectID = cursor.getInt(0);

        cursor.close();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 0, stream);
        byte[] data = stream.toByteArray();

        ContentValues contentValues = new ContentValues();
        contentValues.put("Image", data);
        database.update("Inventories",contentValues, "id=?", new String[]{Integer.toString(projectID)});
    }

}
