package babiaczyk.mateusz.bricklist;

import android.graphics.Bitmap;

public class ProjectItem {
    public String name;
    public String qty;
    public String url;
    public int id;
    public int active;
    public Bitmap image;
}
