package babiaczyk.mateusz.bricklist;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Build;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity {

    private Context mContext;
    RelativeLayout mainWindow;
    private Activity mActivity;
    private ListView mListView;
    private ImageView settings;
    private ImageView addProject;
    private PopupWindow mPopupWindow;
    private TextView appName;
    private TextView projectTextView;
    private ProgressDialog dialog;
    private ProgressBar spinner;

    private String prefixURL;
    private String fullURL;
    ArrayList<ProjectItem> itemList;

    DatabaseAccess db;

    private String imagePrefixURL;

    private int extra;
    private int archive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        isStoragePermissionGranted();

        db = DatabaseAccess.getInstance(this);
        db.open();

        int[] arr;
        arr = db.getSettings();
        extra = arr[0];
        archive = arr[1];

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        dialog = new ProgressDialog(MainActivity.this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Ładowanie pliku. Proszę czekać...");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);


        prefixURL = "http://fcds.cs.put.poznan.pl/MyWeb/BL/";
        imagePrefixURL = "https://img.bricklink.com/ItemImage/SN/0/";
        mContext = getApplicationContext();
        mActivity = MainActivity.this;
        mainWindow = (RelativeLayout) findViewById(R.id.mainWindow);
        mListView = (ListView) findViewById(R.id.item_list_view);

        settings = (ImageView) findViewById(R.id.settings);
        addProject = (ImageView) findViewById(R.id.addProject);
        appName = (TextView) findViewById(R.id.appName);
        projectTextView = (TextView) findViewById(R.id.projectTextView);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Legothick.ttf");
        appName.setTypeface(typeface);
        projectTextView.setTypeface(typeface);


        addProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);



                View customView = inflater.inflate(R.layout.new_project_popup,null);
                mPopupWindow = new PopupWindow(
                        customView,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                );


                if(Build.VERSION.SDK_INT>=21){
                    mPopupWindow.setElevation(5.0f);
                }

                final EditText projectNumber = (EditText) customView.findViewById(R.id.projectNumber);
                Button createButton = (Button) customView.findViewById(R.id.createButton);
                createButton.getBackground().setColorFilter(Color.parseColor("#F98866"), PorterDuff.Mode.MULTIPLY);

                Button closeButton = (Button) customView.findViewById(R.id.closeButton);
                final LinearLayout projectUrl = (LinearLayout) customView.findViewById(R.id.projectUrl);
                final CheckBox urlType = (CheckBox)  customView.findViewById(R.id.urlType);
                urlType.setText("Użyj prefixu: " + prefixURL);
                urlType.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(urlType.isChecked())
                            projectUrl.setVisibility(View.GONE);
                        else
                            projectUrl.setVisibility(View.VISIBLE);
                    }
                });

                TextView newProject = (TextView) customView.findViewById(R.id.textView3);
                Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Legothick.ttf");
                newProject.setTypeface(typeface);


                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPopupWindow.dismiss();
                    }
                });





                createButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        EditText projectNumber = (EditText) mPopupWindow.getContentView().findViewById(R.id.projectNumber);
                        EditText projectName = (EditText) mPopupWindow.getContentView().findViewById(R.id.projectName);
                        EditText projectAddress = (EditText) mPopupWindow.getContentView().findViewById(R.id.projectAddress);
                        final String projNumber = projectNumber.getText().toString();
                        final String projName = projectName.getText().toString();
                        final String projAddress = projectAddress.getText().toString();
                        final boolean uType = urlType.isChecked();
                        if(db.checkProjectName(projName)){
                            if(!projNumber.equalsIgnoreCase("") && !projName.equalsIgnoreCase("") ){
                                dialog.show();
                                Thread one = new Thread() {
                                    public void run() {
                                        if(uType)
                                            fullURL = prefixURL + projNumber + ".xml";
                                        else{
                                            fullURL = projAddress;
                                        }

                                        ArrayList<Item> items = prepareParseXML();
                                        if(items != null){

                                            int qty = countQuantity(items);
                                            Inventory inventory = new Inventory(projName, 1, db.getMaxLastAccessed(), projNumber, qty);
                                            long inventoryID = db.insertInventory(inventory);
                                            db.insertInventoryPart(items, inventoryID);
                                        }
                                        else{
                                            Toast.makeText(mContext, "Czas oczekiwania na dane minął!", Toast.LENGTH_LONG).show();
                                        }
                                        MainActivity.this.runOnUiThread(new Runnable() {
                                            public void run() {
                                                refreshProjects();
                                                dialog.dismiss();
                                                mPopupWindow.dismiss();
                                            }
                                        });

                                    }
                                };

                                one.start();


                            }
                            else{
                                Toast.makeText(mContext, "Żadne z pól nie może być puste!", Toast.LENGTH_LONG).show();
                            }

                        }else{
                            Toast.makeText(mContext, "Taka nazwa już istnieje!", Toast.LENGTH_LONG).show();
                        }




                    }
                });

                mPopupWindow.setFocusable(true);
                mPopupWindow.showAtLocation(mainWindow, Gravity.CENTER,0,-200);


            }
        });


        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.close();
                Intent myIntent = new Intent(mContext, Settings.class);
                myIntent.putExtra("prefixURL", prefixURL);
                Log.e("alala4", Integer.toString(extra) + " " + Integer.toString(archive));
                startActivityForResult(myIntent, 0);
            }
        });

        refreshProjects();

    }

    public void refreshProjects(){
        itemList = db.getInventories(imagePrefixURL, archive==1);


        ItemAdapter adapter = new ItemAdapter(this, itemList, db);
        mListView.setAdapter(adapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProjectItem selectedItem = itemList.get(position);
                Intent myIntent = new Intent(mContext, PartsList.class);
                myIntent.putExtra("projectName", selectedItem.name);
                myIntent.putExtra("projectID", selectedItem.id);
                myIntent.putExtra("active", selectedItem.active);
                startActivityForResult(myIntent, 1);
            }

        });
    }

    public InputStream getInputStream(URL url) {
        try {
            return url.openConnection().getInputStream();
        } catch (IOException e) {
            return null;
        }
    }

    public boolean testInputStream(URL url) {
        try {
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("HEAD");

            con.setConnectTimeout(1500); //set timeout to 5 seconds
            con.setReadTimeout(1500);

            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (java.net.SocketTimeoutException e) {
            return false;
        } catch (java.io.IOException e) {
            return false;
        }
    }

    public ArrayList<Item> prepareParseXML(){
        XmlPullParserFactory pullParserFactory;
        try {
            URL url = new URL(fullURL);
            pullParserFactory = XmlPullParserFactory.newInstance();
            pullParserFactory.setNamespaceAware(false);
            XmlPullParser parser = pullParserFactory.newPullParser();

            if(!testInputStream(url))
                return null;
            InputStream is = getInputStream(url);
            if(is != null){
                parser.setInput(is, "UTF_8");
                return parseXML(parser);
            }
            else{
                Toast.makeText(mContext, "Nie ma takiego zestawu danych :(", Toast.LENGTH_LONG).show();
                return null;
            }


        } catch (XmlPullParserException e) {

            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Item> parseXML(XmlPullParser parser) throws XmlPullParserException,IOException
    {

        ArrayList<Item> items = null;
        int eventType = parser.getEventType();
        Item currentItem = null;
        boolean alternate = true;
        while (eventType != XmlPullParser.END_DOCUMENT){
            String name = null;
            switch (eventType){
                case XmlPullParser.START_DOCUMENT:
                    items = new ArrayList();
                    break;
                case XmlPullParser.START_TAG:
                    name = parser.getName();
                    if (name.equalsIgnoreCase("ITEM")){
                        currentItem = new Item();
                    } else if (currentItem != null){
                        if (name.equalsIgnoreCase("ITEMTYPE")){
                            currentItem.type = parser.nextText();
                        } else if (name.equalsIgnoreCase("ITEMID")){
                            currentItem.id = parser.nextText();
                        } else if (name.equalsIgnoreCase("QTY")){
                            currentItem.qty= parser.nextText();
                        } else if (name.equalsIgnoreCase("COLOR")){
                            currentItem.color= parser.nextText();
                        } else if (name.equalsIgnoreCase("EXTRA")){
                            currentItem.extra = parser.nextText();
                        } else if (name.equalsIgnoreCase("ALTERNATE")){
                            if(!parser.nextText().equalsIgnoreCase("N"))
                                alternate = false;
                        }
                        currentItem.qtyStore = "0";
                    }
                    break;
                case XmlPullParser.END_TAG:
                    name = parser.getName();
                    if (name.equalsIgnoreCase("ITEM") && currentItem != null){
                        if(currentItem.extra.equals("Y"))
                            if(extra == 0)
                                alternate = false;
                        if(alternate)
                            items.add(currentItem);
                        else
                            alternate = true;
                    }
                    break;
            }
            eventType = parser.next();
        }

        //printItems(items);
        return items;
    }

    public int countQuantity(ArrayList<Item> list){
        int sum = 0;
        for(int i=0;i<list.size();i++){
            sum += Integer.parseInt(list.get(i).qty);
        }
        return sum;
    }

    private void printItems(ArrayList<Item> products)
    {
        String content = "";
        Iterator<Item> it = products.iterator();
        while(it.hasNext())
        {
            Item currProduct  = it.next();
            content = content + "Type :" +  currProduct.type + "\n";
            content = content + "ID :" +  currProduct.id + "\n";
            content = content + "Quantity :" +  currProduct.qty + "\n";
            content = content + "Color :" +  currProduct.color + "\n";
            content = content + "Extra :" +  currProduct.extra + "\n";

        }

        Log.e("xmlmati", content);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        db.open();
        if (resultCode == Activity.RESULT_OK) {
            Bundle b = data.getExtras();
            String value = b.getString("prefixURL");
            prefixURL = value;
            int[] tmp = db.getSettings();
            extra = tmp[0];
            archive = tmp[1];

        }
        refreshProjects();
    }


    @Override
    public void finish() {
        db.close();
        super.finish();
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("ALAL","Permission is granted");
                return true;
            } else {

                Log.v("ALAL","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("ALAL","Permission is granted");
            return true;
        }
    }
}
